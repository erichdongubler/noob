use crate::ast::*;

use std::fmt;

impl fmt::Display for Expression<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Expression::Const(i) => write!(f, "{}", i),
            Expression::Ident(id) => write!(f, "{}", id),
            Expression::Boolean(b) => write!(f, "{}", b),
            Expression::MinusPrefix(expr) => write!(f, "(-{})", expr),
            Expression::BangPrefix(expr) => write!(f, "(!{})", expr),
            Expression::Infix(op, left, right) => write!(f, "({} {} {})", left, op, right),
            Expression::If(If {
                condition,
                consequence,
                alternative,
            }) => {
                write!(f, "if {} ", condition)?;
                for statement in consequence.iter() {
                    write!(f, "{} ", statement)?;
                }
                if !alternative.is_empty() {
                    write!(f, "else ")?;
                    for statement in alternative.iter() {
                        write!(f, "{} ", statement)?;
                    }
                }
                Ok(())
            }
            Expression::Function(args, body) => {
                write!(f, "fn ({}) ", args.join(", "))?;
                for statement in body.iter() {
                    write!(f, "{} ", statement)?;
                }
                Ok(())
            }
            Expression::Call(func, args) => {
                let args_string = args
                    .iter()
                    .map(|expr| expr.to_string())
                    .collect::<Vec<String>>()
                    .join(", ");
                write!(f, "{}({})", func, args_string)?;
                Ok(())
            }
        }
    }
}

impl fmt::Display for InfixOp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use InfixOp::*;
        match self {
            Plus => write!(f, "+"),
            Minus => write!(f, "-"),
            Multiply => write!(f, "*"),
            Divide => write!(f, "/"),
            GreaterThan => write!(f, ">"),
            LessThan => write!(f, "<"),
            Equals => write!(f, "=="),
            NotEquals => write!(f, "!="),
        }
    }
}

impl fmt::Display for Statement<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use Statement::*;
        match self {
            Let(name, expr) => write!(f, "let {} = {}", name, expr),
            Return(expr) => write!(f, "return {}", expr),
            Expression(expr) => write!(f, "{}", expr),
        }
    }
}

impl fmt::Display for Program<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for statement in self.statements.iter() {
            writeln!(f, "{}", statement)?;
        }
        Ok(())
    }
}
