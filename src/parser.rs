use crate::ast::{Expression, If, Program, Statement};
use crate::lexer::Lexer;
use crate::token::Token;

use std::convert::From;
use std::iter::Peekable;

#[derive(PartialOrd, PartialEq)]
enum Precedence {
    Lowest,
    Equals,      // ==
    LessGreater, // > or <
    Sum,         // +
    Product,     // *
    Prefix,      // -X or !X
}

impl<'a> From<&Token<'a>> for Precedence {
    fn from(token: &Token<'a>) -> Self {
        use Precedence::*;
        use Token::*;
        match token {
            &Plus | &Minus => Sum,
            &Slash | &Asterisk => Product,
            &Lt | &Gt => LessGreater,
            &Eq | &NotEq => Equals,
            _ => Lowest,
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum ParseError<'input> {
    MissingToken,
    UnexpectedToken(Token<'input>),
}

pub struct Parser<'input> {
    tokens: Peekable<Lexer<'input>>,
}

impl<'input> Parser<'input> {
    pub fn new(lexer: Lexer<'input>) -> Parser<'input> {
        Parser {
            tokens: lexer.peekable(),
        }
    }

    pub fn parse_program(&mut self) -> Result<Program<'input>, ParseError<'input>> {
        Ok(Program {
            statements: self.parse_statements(false)?,
        })
    }

    fn parse_statements(
        &mut self,
        scoped: bool,
    ) -> Result<Vec<Statement<'input>>, ParseError<'input>> {
        let mut statements = Vec::new();

        if scoped {
            self.assert_next_token(Token::Lbrace)?;
        }
        while let Some(token) = self.tokens.peek() {
            let next_statement = match token {
                Token::Let => self.parse_let()?,
                Token::Return => self.parse_return()?,
                Token::Rbrace if scoped => {
                    self.tokens.next();
                    break;
                }
                _ => Statement::Expression(self.parse_expression(Precedence::Lowest)?),
            };

            statements.push(next_statement);
            self.assert_next_token(Token::Semicolon)?;
        }

        Ok(statements)
    }

    fn next_token(&mut self) -> Result<Token<'input>, ParseError<'input>> {
        match self.tokens.next() {
            Some(token) => Ok(token),
            None => Err(ParseError::MissingToken),
        }
    }

    fn assert_next_token(&mut self, token: Token) -> Result<(), ParseError<'input>> {
        let next_token = self.next_token()?;
        if next_token != token {
            return Err(ParseError::UnexpectedToken(next_token));
        }
        Ok(())
    }

    fn parse_let(&mut self) -> Result<Statement<'input>, ParseError<'input>> {
        self.assert_next_token(Token::Let)?;

        let ident_token = self.next_token()?;
        let id = match ident_token {
            Token::Ident(id) => id,
            _ => return Err(ParseError::UnexpectedToken(ident_token)),
        };

        self.assert_next_token(Token::Assign)?;

        let expr = self.parse_expression(Precedence::Lowest)?;

        Ok(Statement::Let(id, expr))
    }

    fn parse_return(&mut self) -> Result<Statement<'input>, ParseError<'input>> {
        self.assert_next_token(Token::Return)?;

        let expr = self.parse_expression(Precedence::Lowest)?;

        Ok(Statement::Return(expr))
    }

    fn parse_expression(
        &mut self,
        precedence: Precedence,
    ) -> Result<Expression<'input>, ParseError<'input>> {
        let mut expr = match self.next_token()? {
            Token::Int(i) => Expression::Const(i),
            Token::Ident(id) => Expression::Ident(id),
            Token::True => Expression::Boolean(true),
            Token::False => Expression::Boolean(false),
            Token::Bang => {
                Expression::BangPrefix(Box::new(self.parse_expression(Precedence::Prefix)?))
            }
            Token::Minus => {
                Expression::MinusPrefix(Box::new(self.parse_expression(Precedence::Prefix)?))
            }
            Token::Lparen => {
                let expr = self.parse_expression(Precedence::Lowest)?;
                self.assert_next_token(Token::Rparen)?;
                expr
            }
            Token::If => {
                self.assert_next_token(Token::Lparen)?;
                let condition = Box::new(self.parse_expression(Precedence::Lowest)?);
                self.assert_next_token(Token::Rparen)?;

                let consequence = self.parse_statements(true)?;

                let alternative = if self.tokens.peek() == Some(&Token::Else) {
                    self.next_token()?; // consume else
                    self.parse_statements(true)?
                } else {
                    Vec::new()
                };

                Expression::If(If {
                    condition,
                    consequence,
                    alternative,
                })
            }
            Token::Function => {
                self.assert_next_token(Token::Lparen)?;
                let mut args = vec![];
                for token in self.tokens.by_ref() {
                    if token == Token::Rparen {
                        break;
                    }
                    match token {
                        Token::Ident(id) => args.push(id),
                        Token::Comma => continue,
                        token => return Err(ParseError::UnexpectedToken(token)),
                    }
                }

                let body = self.parse_statements(true)?;

                Expression::Function(args, body)
            }
            token => return Err(ParseError::UnexpectedToken(token)),
        };

        loop {
            match self.tokens.peek() {
                Some(&Token::Lparen) => {
                    // parse the function call
                    self.next_token()?; // consume "("
                    let mut args = vec![];
                    loop {
                        let token = self.tokens.peek();
                        if token == Some(&Token::Rparen) {
                            self.next_token()?; // consume ")"
                            break;
                        }
                        match token {
                            Some(&Token::Comma) => {
                                self.next_token()?; // consume ","
                                continue;
                            }
                            Some(_) => args.push(self.parse_expression(Precedence::Lowest)?),
                            None => return Err(ParseError::MissingToken),
                        }
                    }
                    expr = Expression::Call(Box::new(expr), args);
                }
                Some(token) if precedence < token.into() => {
                    expr = self.parse_infix(expr)?;
                }
                _ => {
                    break;
                }
            }
        }

        Ok(expr)
    }

    fn parse_infix(
        &mut self,
        left_expr: Expression<'input>,
    ) -> Result<Expression<'input>, ParseError<'input>> {
        let operator_token = self.next_token()?;

        Ok(Expression::Infix(
            (&operator_token).into(),
            Box::new(left_expr),
            Box::new(self.parse_expression((&operator_token).into())?),
        ))
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::ast::{Expression, InfixOp, Statement};

    #[test]
    fn let_statement() {
        let input = "\
                     let x = 5;\
                     let y = 10;\
                     let foobar = 838383;\
                     ";
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program();
        assert!(program.is_ok());

        let program = program.unwrap();
        assert_eq!(program.statements.len(), 3);

        assert_eq!(
            program.statements,
            vec![
                Statement::Let("x", Expression::Const(5)),
                Statement::Let("y", Expression::Const(10)),
                Statement::Let("foobar", Expression::Const(838_383))
            ]
        );
    }

    #[test]
    fn let_statement_with_errors() {
        let input = "\
                     let x 5;\
                     ";
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program();

        assert_eq!(program, Err(ParseError::UnexpectedToken(Token::Int(5))));
    }

    #[test]
    fn print_let_statement() {
        let input = "\
                     let x = 5;\
                     return 10; 
                     ";
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program().unwrap();
        let s = program.to_string();
        assert_eq!(s, "let x = 5\nreturn 10\n");
    }

    #[test]
    fn simple_expression_statements() {
        let input = "\
                     foobar;
                     5;
                     ";
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program().unwrap();
        assert_eq!(program.statements.len(), 2);
        assert_eq!(
            program.statements[0],
            Statement::Expression(Expression::Ident("foobar"))
        );
        assert_eq!(
            program.statements[1],
            Statement::Expression(Expression::Const(5))
        );
    }

    #[test]
    fn prefix() {
        let input = "\
                     !5;
                     -15;
                     ";
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program().unwrap();

        assert_eq!(program.statements.len(), 2);

        assert_eq!(
            program.statements[0],
            Statement::Expression(Expression::BangPrefix(Box::new(Expression::Const(5))))
        );
        assert_eq!(
            program.statements[1],
            Statement::Expression(Expression::MinusPrefix(Box::new(Expression::Const(15))))
        );
    }

    #[test]
    fn infix() {
        let input = "\
                     5 + 10;
                     5 - 10;
                     5 * 10;
                     5 / 10;
                     5 > 10;
                     5 < 10;
                     5 == 10;
                     5 != 10;
                     ";
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program().unwrap();

        assert_eq!(
            program.statements,
            vec![
                Statement::Expression(Expression::Infix(
                    InfixOp::Plus,
                    Box::new(Expression::Const(5)),
                    Box::new(Expression::Const(10))
                )),
                Statement::Expression(Expression::Infix(
                    InfixOp::Minus,
                    Box::new(Expression::Const(5)),
                    Box::new(Expression::Const(10))
                )),
                Statement::Expression(Expression::Infix(
                    InfixOp::Multiply,
                    Box::new(Expression::Const(5)),
                    Box::new(Expression::Const(10))
                )),
                Statement::Expression(Expression::Infix(
                    InfixOp::Divide,
                    Box::new(Expression::Const(5)),
                    Box::new(Expression::Const(10))
                )),
                Statement::Expression(Expression::Infix(
                    InfixOp::GreaterThan,
                    Box::new(Expression::Const(5)),
                    Box::new(Expression::Const(10))
                )),
                Statement::Expression(Expression::Infix(
                    InfixOp::LessThan,
                    Box::new(Expression::Const(5)),
                    Box::new(Expression::Const(10))
                )),
                Statement::Expression(Expression::Infix(
                    InfixOp::Equals,
                    Box::new(Expression::Const(5)),
                    Box::new(Expression::Const(10))
                )),
                Statement::Expression(Expression::Infix(
                    InfixOp::NotEquals,
                    Box::new(Expression::Const(5)),
                    Box::new(Expression::Const(10))
                ))
            ]
        );
    }

    #[test]
    fn infix_with_precedence() {
        let input = "\
                     3 + 4 * 5 == 3 * 1 + 4 * 5;
                     5 < 4 != 3 > 4;
                     a + b / c;
                     ";
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program().unwrap();
        assert_eq!(
            program.to_string(),
            "\
((3 + (4 * 5)) == ((3 * 1) + (4 * 5)))
((5 < 4) != (3 > 4))
(a + (b / c))\n"
        );
    }

    #[test]
    fn booleans() {
        let input = "!true == false;";
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program().unwrap();
        assert_eq!(
            program.statements[0],
            Statement::Expression(Expression::Infix(
                InfixOp::Equals,
                Box::new(Expression::BangPrefix(Box::new(Expression::Boolean(true)))),
                Box::new(Expression::Boolean(false))
            ))
        );
    }

    #[test]
    fn parens() {
        let input = "1 + (2 + 3);";
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program().unwrap();
        assert_eq!(
            program.statements[0],
            Statement::Expression(Expression::Infix(
                InfixOp::Plus,
                Box::new(Expression::Const(1)),
                Box::new(Expression::Infix(
                    InfixOp::Plus,
                    Box::new(Expression::Const(2)),
                    Box::new(Expression::Const(3))
                )),
            ))
        );
    }

    #[test]
    fn parse_if() {
        let input = "if (5) { 6; };";
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program().unwrap();
        assert_eq!(
            program.statements[0],
            Statement::Expression(Expression::If(If {
                condition: Box::new(Expression::Const(5)),
                consequence: vec![Statement::Expression(Expression::Const(6))],
                alternative: vec![]
            }))
        );
    }

    #[test]
    fn parse_if_else() {
        let input = "if (5) { 6; } else { 7; };";
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program().unwrap();
        assert_eq!(
            program.statements[0],
            Statement::Expression(Expression::If(If {
                condition: Box::new(Expression::Const(5)),
                consequence: vec![Statement::Expression(Expression::Const(6))],
                alternative: vec![Statement::Expression(Expression::Const(7))]
            }))
        );
    }

    #[test]
    fn function_expr() {
        let input = "let myFunc = fn(x, y) {x + y;};";
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program().unwrap();
        assert_eq!(
            program.statements[0],
            Statement::Let(
                "myFunc",
                Expression::Function(
                    vec!["x", "y"],
                    vec![Statement::Expression(Expression::Infix(
                        InfixOp::Plus,
                        Box::new(Expression::Ident("x")),
                        Box::new(Expression::Ident("y"))
                    ))]
                )
            )
        );
    }

    #[test]
    fn function_call_expr() {
        let input = "
                     add(1, 2);
                     fn(x, y) { x + y; }(2, 3);
                     ";
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program().unwrap();

        assert_eq!(
            program.statements[0],
            Statement::Expression(Expression::Call(
                Box::new(Expression::Ident("add")),
                vec![Expression::Const(1), Expression::Const(2)]
            ))
        );

        assert_eq!(
            program.statements[1],
            Statement::Expression(Expression::Call(
                Box::new(Expression::Function(
                    vec!["x", "y"],
                    vec![Statement::Expression(Expression::Infix(
                        InfixOp::Plus,
                        Box::new(Expression::Ident("x")),
                        Box::new(Expression::Ident("y"))
                    ))]
                )),
                vec![Expression::Const(2), Expression::Const(3)]
            ))
        );
    }

    #[test]
    fn function_call_to_string() {
        let input = "\
                     a + add(b * c) + d;
                     add(a, b, 1, 2 * 3, 4 + 5, add(6, 7 * 8));
                     add(a + b + c * d / f + g);
                     ";
        let lexer = Lexer::new(input);
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program().unwrap();
        assert_eq!(
            program.to_string(),
            "\
((a + add((b * c))) + d)
add(a, b, 1, (2 * 3), (4 + 5), add(6, (7 * 8)))
add((((a + b) + ((c * d) / f)) + g))\n"
        );
    }
}
