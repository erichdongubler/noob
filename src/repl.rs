use crate::lexer::Lexer;
use crate::parser::Parser;

use std::io::{self, Write};

const FACE: &str = r#"
      ***      
    *******    
   *********   
/\* ### ### */\
|    @ / @    |
\/\    ^    /\/
   \  ===  /   
    \_____/    
     _|_|_     
  *$$$$$$$$$* 
"#;

pub fn start() {
    loop {
        print!(">> ");
        io::stdout().flush().unwrap();

        let mut buffer = String::new();
        io::stdin().read_line(&mut buffer).unwrap();
        let lexer = Lexer::new(&buffer);
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program();
        match program {
            Ok(program) => println!("{}", program.to_string()),
            Err(e) => {
                println!("{}", FACE);
                println!("Noob found an error: {:?}", e)
            }
        }
    }
}
