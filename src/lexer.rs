use crate::token::{lookup_identifier, Token};

use std::iter::Peekable;
use std::str::Chars;

fn is_letter(ch: char) -> bool {
    ch.is_alphabetic() || ch == '_'
}

fn is_number(ch: char) -> bool {
    ch.is_numeric()
}

fn is_whitespace(ch: char) -> bool {
    ch.is_whitespace()
}

pub struct Lexer<'input> {
    // The string to parse.
    input: &'input str,

    // The starting index of the next character.
    index: usize,

    // The iterator to extract characters from the input.
    chars: Peekable<Chars<'input>>,
}

impl<'input> Lexer<'input> {
    /// Creates a new lexer for the given input.
    pub fn new(input: &'input str) -> Lexer {
        Lexer {
            input,
            index: 0,
            chars: input.chars().peekable(),
        }
    }

    /// Returns the next character and updates the index.
    fn next_char(&mut self) -> Option<char> {
        let ch = self.chars.next();

        if let Some(c) = ch {
            self.index += c.len_utf8();
        }
        ch
    }

    /// Reads a batch of characters so long as the predicate yields true.
    fn select<F>(&mut self, predicate: F) -> &'input str
    where
        F: Fn(char) -> bool,
    {
        let index = self.index;
        while let Some(&ch) = self.chars.peek() {
            if !predicate(ch) {
                break;
            }
            self.next_char();
        }
        &self.input[index..self.index]
    }
}

impl<'input> Iterator for Lexer<'input> {
    type Item = Token<'input>;

    fn next(&mut self) -> Option<Self::Item> {
        self.select(is_whitespace);

        let next_char = self.chars.peek().cloned();
        next_char.map(|ch| {
            if is_letter(ch) {
                let ident = self.select(is_letter);
                lookup_identifier(ident)
            } else if is_number(ch) {
                let number = self
                    .select(is_number)
                    .parse::<i32>()
                    .expect("Error parsing an integer");
                Token::Int(number)
            } else {
                // at this point we have to advance the iterator
                let ch = self.next_char().unwrap();
                match ch {
                    '=' => {
                        if self.chars.peek() == Some(&'=') {
                            self.next_char();
                            Token::Eq
                        } else {
                            Token::Assign
                        }
                    }
                    '+' => Token::Plus,
                    '-' => Token::Minus,
                    '!' => {
                        if self.chars.peek() == Some(&'=') {
                            self.next_char();
                            Token::NotEq
                        } else {
                            Token::Bang
                        }
                    }
                    '*' => Token::Asterisk,
                    '/' => Token::Slash,
                    '<' => Token::Lt,
                    '>' => Token::Gt,
                    ',' => Token::Comma,
                    ';' => Token::Semicolon,
                    '(' => Token::Lparen,
                    ')' => Token::Rparen,
                    '{' => Token::Lbrace,
                    '}' => Token::Rbrace,
                    _ => Token::Illegal(ch),
                }
            }
        })
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn next_token() {
        let input = "
        let five = 5;
        let ten = 10;

        let add = fn(x, y) {
           x + y;
        };

        let result = add(five, ten);
        !-/*5;
        5 < 10 > 5;
        
        if (5 < 10) {
            return true;
        } else {
            return false;
        }
        
        10 == 10;
        10 != 9;
        ";

        let mut l = Lexer::new(input);
        assert_eq!(l.next().unwrap(), Token::Let);
        assert_eq!(l.next().unwrap(), Token::Ident("five"));
        assert_eq!(l.next().unwrap(), Token::Assign);
        assert_eq!(l.next().unwrap(), Token::Int(5));
        assert_eq!(l.next().unwrap(), Token::Semicolon);

        assert_eq!(l.next().unwrap(), Token::Let);
        assert_eq!(l.next().unwrap(), Token::Ident("ten"));
        assert_eq!(l.next().unwrap(), Token::Assign);
        assert_eq!(l.next().unwrap(), Token::Int(10));
        assert_eq!(l.next().unwrap(), Token::Semicolon);

        assert_eq!(l.next().unwrap(), Token::Let);
        assert_eq!(l.next().unwrap(), Token::Ident("add"));
        assert_eq!(l.next().unwrap(), Token::Assign);
        assert_eq!(l.next().unwrap(), Token::Function);
        assert_eq!(l.next().unwrap(), Token::Lparen);
        assert_eq!(l.next().unwrap(), Token::Ident("x"));
        assert_eq!(l.next().unwrap(), Token::Comma);
        assert_eq!(l.next().unwrap(), Token::Ident("y"));
        assert_eq!(l.next().unwrap(), Token::Rparen);
        assert_eq!(l.next().unwrap(), Token::Lbrace);
        assert_eq!(l.next().unwrap(), Token::Ident("x"));
        assert_eq!(l.next().unwrap(), Token::Plus);
        assert_eq!(l.next().unwrap(), Token::Ident("y"));
        assert_eq!(l.next().unwrap(), Token::Semicolon);
        assert_eq!(l.next().unwrap(), Token::Rbrace);
        assert_eq!(l.next().unwrap(), Token::Semicolon);

        assert_eq!(l.next().unwrap(), Token::Let);
        assert_eq!(l.next().unwrap(), Token::Ident("result"));
        assert_eq!(l.next().unwrap(), Token::Assign);
        assert_eq!(l.next().unwrap(), Token::Ident("add"));
        assert_eq!(l.next().unwrap(), Token::Lparen);
        assert_eq!(l.next().unwrap(), Token::Ident("five"));
        assert_eq!(l.next().unwrap(), Token::Comma);
        assert_eq!(l.next().unwrap(), Token::Ident("ten"));
        assert_eq!(l.next().unwrap(), Token::Rparen);
        assert_eq!(l.next().unwrap(), Token::Semicolon);

        assert_eq!(l.next().unwrap(), Token::Bang);
        assert_eq!(l.next().unwrap(), Token::Minus);
        assert_eq!(l.next().unwrap(), Token::Slash);
        assert_eq!(l.next().unwrap(), Token::Asterisk);
        assert_eq!(l.next().unwrap(), Token::Int(5));
        assert_eq!(l.next().unwrap(), Token::Semicolon);
        assert_eq!(l.next().unwrap(), Token::Int(5));
        assert_eq!(l.next().unwrap(), Token::Lt);
        assert_eq!(l.next().unwrap(), Token::Int(10));
        assert_eq!(l.next().unwrap(), Token::Gt);
        assert_eq!(l.next().unwrap(), Token::Int(5));
        assert_eq!(l.next().unwrap(), Token::Semicolon);

        assert_eq!(l.next().unwrap(), Token::If);
        assert_eq!(l.next().unwrap(), Token::Lparen);
        assert_eq!(l.next().unwrap(), Token::Int(5));
        assert_eq!(l.next().unwrap(), Token::Lt);
        assert_eq!(l.next().unwrap(), Token::Int(10));
        assert_eq!(l.next().unwrap(), Token::Rparen);
        assert_eq!(l.next().unwrap(), Token::Lbrace);
        assert_eq!(l.next().unwrap(), Token::Return);
        assert_eq!(l.next().unwrap(), Token::True);
        assert_eq!(l.next().unwrap(), Token::Semicolon);
        assert_eq!(l.next().unwrap(), Token::Rbrace);
        assert_eq!(l.next().unwrap(), Token::Else);
        assert_eq!(l.next().unwrap(), Token::Lbrace);
        assert_eq!(l.next().unwrap(), Token::Return);
        assert_eq!(l.next().unwrap(), Token::False);
        assert_eq!(l.next().unwrap(), Token::Semicolon);
        assert_eq!(l.next().unwrap(), Token::Rbrace);

        assert_eq!(l.next().unwrap(), Token::Int(10));
        assert_eq!(l.next().unwrap(), Token::Eq);
        assert_eq!(l.next().unwrap(), Token::Int(10));
        assert_eq!(l.next().unwrap(), Token::Semicolon);
        assert_eq!(l.next().unwrap(), Token::Int(10));
        assert_eq!(l.next().unwrap(), Token::NotEq);
        assert_eq!(l.next().unwrap(), Token::Int(9));
        assert_eq!(l.next().unwrap(), Token::Semicolon);
    }
}
