mod display;

use crate::token::Token;

#[derive(Debug, PartialEq)]
pub enum Expression<'a> {
    Const(i32),
    Ident(&'a str),
    Boolean(bool),
    MinusPrefix(Box<Expression<'a>>),
    BangPrefix(Box<Expression<'a>>),
    Infix(InfixOp, Box<Expression<'a>>, Box<Expression<'a>>),
    If(If<'a>),
    Function(Vec<&'a str>, Vec<Statement<'a>>),
    Call(Box<Expression<'a>>, Vec<Expression<'a>>),
}

#[derive(Debug, PartialEq)]
pub struct If<'a> {
    pub condition: Box<Expression<'a>>,
    pub consequence: Vec<Statement<'a>>,
    pub alternative: Vec<Statement<'a>>,
}

#[derive(Debug, PartialEq)]
pub enum InfixOp {
    Plus,
    Minus,
    Multiply,
    Divide,
    GreaterThan,
    LessThan,
    Equals,
    NotEquals,
}

impl<'a> From<&Token<'a>> for InfixOp {
    fn from(token: &Token<'a>) -> Self {
        match *token {
            Token::Plus => InfixOp::Plus,
            Token::Minus => InfixOp::Minus,
            Token::Slash => InfixOp::Divide,
            Token::Asterisk => InfixOp::Multiply,
            Token::Lt => InfixOp::LessThan,
            Token::Gt => InfixOp::GreaterThan,
            Token::Eq => InfixOp::Equals,
            Token::NotEq => InfixOp::NotEquals,
            _ => panic!("Token to Infix Operator conversion undefined"),
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum Statement<'a> {
    Let(&'a str, Expression<'a>),
    Return(Expression<'a>),
    Expression(Expression<'a>),
}

#[derive(Debug, PartialEq)]
pub struct Program<'a> {
    pub statements: Vec<Statement<'a>>,
}
